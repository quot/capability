= Capability

//tag::abstract[]

Commonly  containers are running with a lot more privileges than they need.
Dropping these capabilities when the containers are in production
decrease the attack surface on the environment.

//end::abstract[]

Docker by default gives the following capabilities to the containers.

[source]
----
cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_admin,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
----

These default capabilities are not necessary.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

image::https://gitlab.com/insecure-programming/docker/capability/badges/master/pipeline.svg[]

=== Task 0

Fork and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
Find out the security issue and why tests fails.
Do you think the default capabilities for client
and server are necessary?

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

Edit `docker-compose.yml` and drop unnecessary capabilities
and only add the necessary ones.

Note: Only add and remove capabilities and keep
the remaining of `docker-compose.yml` unchanged.

== Task 3

Run security tests again. Make sure build pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the changes. 
Run all tests and make sure everything pass. 


=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)


== Bonus

. Can you think of another capability that can be removed from the server?

//end:lab[]

//tag::references[]

== References

* https://www.redhat.com/en/blog/secure-your-containers-one-weird-trick
* https://stackoverflow.com/questions/43467670/which-capabilities-can-i-drop-in-a-docker-nginx-container
* https://developers.redhat.com/blog/2017/02/16/find-what-capabilities-an-application-requires-to-successful-run-in-a-container/
* 
* https://danwalsh.livejournal.com/76358.html
* https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities

//end::references[]
